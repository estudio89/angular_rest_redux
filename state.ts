import { BaseModel } from "./base_model";
import {
  SendRequestAction,
  FetchItemsAction,
  CreateItemAction,
  RetrieveItemAction,
  UpdateItemAction,
  DeleteItemAction,
  BulkCreateItemsAction,
} from "./actions";

export interface IAppState {
  restState: RestState;
}

export class RestState {
  resourceState: { [resourceName: string]: ResourceState };
  oneOffRequestState: { [resourceName: string]: OneOffRequestState };

  static initial(): RestState {
    return {
      resourceState: {} as { [resourceName: string]: ResourceState },
      oneOffRequestState: {} as { [resourceName: string]: OneOffRequestState },
    };
  }
}

export class ResourceState {
  itemId: string | undefined;
  itemsById: { [itemId: string]: BaseModel };
  itemIds: string[];
  items: BaseModel[];
  totalItemsCount: number;

  resourceFetchState: ResourceFetchState;
  resourceCreateState: ResourceCreateState; // FAZER ALTERAR CONTAGENS
  resourceBulkCreateState: ResourceBulkCreateState;
  resourceRetrieveState: ResourceRetrieveState;
  resourceUpdateState: ResourceUpdateState;
  resourceDeleteState: ResourceDeleteState;

  constructor() {
    this.itemsById = {} as { string: BaseModel };
    this.itemIds = [];
    this.items = [];
    this.totalItemsCount = 0;
    this.resourceFetchState = new ResourceFetchState();
    this.resourceCreateState = new ResourceCreateState();
    this.resourceBulkCreateState = new ResourceBulkCreateState();
    this.resourceRetrieveState = new ResourceRetrieveState();
    this.resourceUpdateState = new ResourceUpdateState();
    this.resourceDeleteState = new ResourceDeleteState();
  }
}

export class OneOffRequestState {
  isRunning: boolean;
  result: any;
  exception: any;
  failedAction: SendRequestAction;
}

export class ResourceFetchState {
  exception: any;
  failedAction: FetchItemsAction;
  loadedItems: boolean;
  isRunning: boolean;
  currentPage: number;
  currentParams: { [key: string]: string };

  constructor() {
    this.loadedItems = false;
    this.isRunning = false;
    this.currentPage = -1;
    this.currentParams = {};
  }
}

export class ResourceCreateState {
  isRunning: boolean;
  exception: any;
  failedAction: CreateItemAction;
}

export class ResourceBulkCreateState {
  isRunning: boolean;
  exception: any;
  failedAction: BulkCreateItemsAction;
}

export class ResourceRetrieveState {
  isRunning: boolean;
  exception: any;
  failedAction: RetrieveItemAction;
}

export class ResourceUpdateState {
  isRunning: boolean;
  exception: any;
  failedAction: UpdateItemAction;
}

export class ResourceDeleteState {
  isRunning: boolean;
  exception: any;
  failedAction: DeleteItemAction;
}
