import {
  OneOffRequestState,
  ResourceState,
  ResourceFetchState,
  ResourceCreateState,
  ResourceRetrieveState,
  ResourceUpdateState,
  ResourceDeleteState,
  IAppState,
  ResourceBulkCreateState,
} from "./state";
import {
  SendRequestAction,
  FetchItemsAction,
  CreateItemAction,
  RetrieveItemAction,
  UpdateItemAction,
  DeleteItemAction,
  RefreshOptions,
  BulkCreateItemsAction,
} from "./actions";
import { BaseModel } from "./base_model";
import { IAPIMapping, PaginationType } from "./config";
import { Injectable } from "@angular/core";
import * as _ from "lodash";

function _getResourceState(
  state: IAppState,
  resourceName: string
): ResourceState {
  const ret =
    state.restState.resourceState[resourceName] || new ResourceState();
  return ret;
}

function _getResourceStatePath(resourceName: string): string[] {
  return ["restState", "resourceState", resourceName];
}

// ONE-OFF REQUESTS
@Injectable()
export class OneOffSelectors {
  getOneOffRequestState(
    state: IAppState,
    requestId: string
  ): OneOffRequestState {
    return (
      state.restState.oneOffRequestState[requestId] || new OneOffRequestState()
    );
  }

  isRunning(state: IAppState, requestId: string): boolean {
    const oneOffState = this.getOneOffRequestState(state, requestId);
    return oneOffState.isRunning;
  }

  failedRequest(state: IAppState, requestId: string): boolean {
    const oneOffState = this.getOneOffRequestState(state, requestId);
    return oneOffState.exception != null;
  }

  getFailedRequestAction(
    state: IAppState,
    requestId: string
  ): SendRequestAction {
    const oneOffState = this.getOneOffRequestState(state, requestId);
    return oneOffState.failedAction;
  }

  getRequestResult<T>(state: IAppState, requestId: string): T {
    const oneOffState = this.getOneOffRequestState(state, requestId);
    return oneOffState.result;
  }
}

// FETCH
@Injectable()
export class FetchSelectors {
  constructor(private apiMapping: IAPIMapping) {}

  public getFetchState(
    state: IAppState,
    resourceName: string
  ): ResourceFetchState {
    return _getResourceState(state, resourceName).resourceFetchState;
  }

  public getFetchStatePath(resourceName: string) {
    return _getResourceStatePath(resourceName).concat("resourceFetchState");
  }

  public isFetching(state: IAppState, resourceName: string): boolean {
    const fetchState = this.getFetchState(state, resourceName);
    if (
      this.apiMapping.getConfig(resourceName).paginationType ===
      PaginationType.ALWAYS_APPEND
    ) {
      return fetchState.isRunning && !this.itemsWereLoaded(state, resourceName);
    } else {
      return fetchState.isRunning;
    }
  }

  public isFetchingPath(resourceName: string) {
    return this.getFetchStatePath(resourceName).concat("isRunning");
  }

  public isFetchingMore(state: IAppState, resourceName: string): boolean {
    const fetchState = this.getFetchState(state, resourceName);
    return fetchState.isRunning && this.itemsWereLoaded(state, resourceName);
  }

  public getItems<T extends BaseModel>(
    state: IAppState,
    resourceName: string
  ): T[] {
    const resourceState = _getResourceState(state, resourceName);
    return _.cloneDeep(resourceState.items as T[]);
  }

  public failedFetching(state: IAppState, resourceName: string): boolean {
    const fetchState = this.getFetchState(state, resourceName);
    return fetchState.exception != null;
  }

  public getFetchException(state: IAppState, resourceName: string): any {
    const fetchState = this.getFetchState(state, resourceName);
    return fetchState.exception;
  }

  public getFailedFetchAction(
    state: IAppState,
    resourceName: string
  ): FetchItemsAction {
    const fetchState = this.getFetchState(state, resourceName);
    return fetchState.failedAction;
  }

  public itemsWereLoaded(state: IAppState, resourceName: string): boolean {
    const fetchState = this.getFetchState(state, resourceName);
    return fetchState.loadedItems;
  }

  public _hasMoreToFetch(state: IAppState, resourceName: string): boolean {
    const resourceState = _getResourceState(state, resourceName);
    const totalItemsCount = resourceState.totalItemsCount;
    const itemIds: string[] = resourceState.itemIds;
    return totalItemsCount === -1 || itemIds.length < totalItemsCount;
  }

  public canFetchMore(state: IAppState, resourceName: string): boolean {
    const fetchState = this.getFetchState(state, resourceName);
    const _isFetching = fetchState.isRunning;
    const moreToFetch = this._hasMoreToFetch(state, resourceName);
    return moreToFetch && !_isFetching;
  }

  public canFetch(
    state: IAppState,
    resourceName: string,
    refresh: RefreshOptions
  ): boolean {
    const fetchState = this.getFetchState(state, resourceName);
    const _isFetching = fetchState.isRunning;
    const _itemsWereLoaded = this.itemsWereLoaded(state, resourceName);

    if (_isFetching) {
      return false;
    } else if (
      refresh === RefreshOptions.FULL ||
      refresh === RefreshOptions.BACKGROUND
    ) {
      return true;
    } else {
      if (
        this.apiMapping.getConfig(resourceName).paginationType ===
        PaginationType.ALWAYS_APPEND
      ) {
        return !_itemsWereLoaded;
      } else {
        return true;
      }
    }
  }

  public getCurrentPage(state: IAppState, resourceName: string): number {
    const fetchState = this.getFetchState(state, resourceName);
    return fetchState.currentPage;
  }

  public getCurrentParams(
    state: IAppState,
    resourceName: string
  ): { [key: string]: string } {
    const fetchState = this.getFetchState(state, resourceName);
    return fetchState.currentParams;
  }

  public getNextPage(
    state: IAppState,
    resourceName: string,
    refresh: RefreshOptions
  ): number {
    if (refresh == RefreshOptions.FULL) {
      return 0;
    }
    const fetchState = this.getFetchState(state, resourceName);
    const currentPage = fetchState.currentPage;
    return currentPage + 1;
  }

  public getTotalItemsCount(state: IAppState, resourceName: string): number {
    const resourceState = _getResourceState(state, resourceName);
    return resourceState.totalItemsCount;
  }
}

// CREATE/RETRIEVE/UPDATE
export class CRUSelectors {
  getDetailItem(state: IAppState, resourceName: string): BaseModel | undefined {
    const resourceState = _getResourceState(state, resourceName);
    const itemId = resourceState.itemId;
    if (!itemId) {
      return undefined;
    }

    return this.getItem(state, resourceName, itemId);
  }

  getItem(state: IAppState, resourceName: string, itemId: string): BaseModel {
    const resourceState = _getResourceState(state, resourceName);
    const itemsById: { [itemId: string]: BaseModel } = resourceState.itemsById;
    const item = itemsById[itemId];

    if (item == null) {
      throw new Error(`Item with id \"${itemId}\" was not found in mapping.`);
    }

    return item;
  }
}

// CREATE
@Injectable()
export class CreateSelectors extends CRUSelectors {
  getCreateState(state: IAppState, resourceName: string): ResourceCreateState {
    return _getResourceState(state, resourceName).resourceCreateState;
  }

  getBulkCreateState(
    state: IAppState,
    resourceName: string
  ): ResourceBulkCreateState {
    return _getResourceState(state, resourceName).resourceBulkCreateState;
  }

  isCreating(state: IAppState, resourceName: string): boolean {
    const createState = this.getCreateState(state, resourceName);
    return createState.isRunning;
  }

  isBulkCreating(state: IAppState, resourceName: string): boolean {
    const bulkCreateState = this.getBulkCreateState(state, resourceName);
    return bulkCreateState.isRunning;
  }

  failedCreating(state: IAppState, resourceName: string): boolean {
    const createState = this.getCreateState(state, resourceName);
    return createState.exception != null;
  }

  failedBulkCreating(state: IAppState, resourceName: string): boolean {
    const bulkCreateState = this.getBulkCreateState(state, resourceName);
    return bulkCreateState.exception != null;
  }

  getCreateException(state: IAppState, resourceName: string): any {
    const createState = this.getCreateState(state, resourceName);
    return createState.exception;
  }

  getBulkCreateException(state: IAppState, resourceName: string): any {
    const bulkCreateState = this.getBulkCreateState(state, resourceName);
    return bulkCreateState.exception;
  }

  getFailedCreateAction(
    state: IAppState,
    resourceName: string
  ): CreateItemAction {
    const createState = this.getCreateState(state, resourceName);
    return createState.failedAction;
  }

  getFailedBulkCreateAction(
    state: IAppState,
    resourceName: string
  ): BulkCreateItemsAction {
    const bulkCreateState = this.getBulkCreateState(state, resourceName);
    return bulkCreateState.failedAction;
  }
}

// RETRIEVE
@Injectable()
export class RetrieveSelectors extends CRUSelectors {
  getRetrieveState(
    state: IAppState,
    resourceName: string
  ): ResourceRetrieveState {
    return _getResourceState(state, resourceName).resourceRetrieveState;
  }

  isRetrieving(state: IAppState, resourceName: string): boolean {
    const resourceRetrieveState = this.getRetrieveState(state, resourceName);
    return resourceRetrieveState.isRunning;
  }

  failedRetrieving(state: IAppState, resourceName: string): boolean {
    const resourceRetrieveState = this.getRetrieveState(state, resourceName);
    return resourceRetrieveState.exception != null;
  }

  getRetrieveException(state: IAppState, resourceName: string): any {
    const resourceRetrieveState = this.getRetrieveState(state, resourceName);
    return resourceRetrieveState.exception;
  }

  getFailedRetrieveAction(
    state: IAppState,
    resourceName: string
  ): RetrieveItemAction {
    const retrieveState = this.getRetrieveState(state, resourceName);
    return retrieveState.failedAction;
  }
}

// UPDATE
@Injectable()
export class UpdateSelectors extends CRUSelectors {
  getUpdateState(state: IAppState, resourceName: string): ResourceUpdateState {
    return _getResourceState(state, resourceName).resourceUpdateState;
  }

  isUpdating(state: IAppState, resourceName: string): boolean {
    const resourceUpdateState = this.getUpdateState(state, resourceName);
    return resourceUpdateState.isRunning;
  }

  failedUpdating(state: IAppState, resourceName: string): boolean {
    const resourceUpdateState = this.getUpdateState(state, resourceName);
    return resourceUpdateState.exception != null;
  }

  getUpdateException(state: IAppState, resourceName: string): any {
    const resourceUpdateState = this.getUpdateState(state, resourceName);
    return resourceUpdateState.exception;
  }

  getFailedUpdateAction(
    state: IAppState,
    resourceName: string
  ): UpdateItemAction {
    const updateState = this.getUpdateState(state, resourceName);
    return updateState.failedAction;
  }
}

// DELETE
@Injectable()
export class DeleteSelectors {
  getDeleteState(state: IAppState, resourceName: string): ResourceDeleteState {
    return _getResourceState(state, resourceName).resourceDeleteState;
  }

  isDeleting(state: IAppState, resourceName: string): boolean {
    const resourceDeleteState = this.getDeleteState(state, resourceName);
    return resourceDeleteState.isRunning;
  }

  wasDeleted(state: IAppState, resourceName: string, item: BaseModel): boolean {
    const resourceState = _getResourceState(state, resourceName);
    return resourceState.itemIds.indexOf(item.id) === -1;
  }

  failedDeleting(state: IAppState, resourceName: string): boolean {
    const resourceDeleteState = this.getDeleteState(state, resourceName);
    return resourceDeleteState.exception != null;
  }

  getDeleteException(state: IAppState, resourceName: string): any {
    const resourceDeleteState = this.getDeleteState(state, resourceName);
    return resourceDeleteState.exception;
  }

  getFailedDeleteAction(
    state: IAppState,
    resourceName: string
  ): DeleteItemAction {
    const deleteState = this.getDeleteState(state, resourceName);
    return deleteState.failedAction;
  }
}
