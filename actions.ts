import { BaseModel } from "./base_model";
import { PromiseCompleter, PromiseWrapper } from "./utils";
import { Action, Store } from "redux";
import { Injectable } from "@angular/core";
import { PaginationType, IAPIMapping } from "./config";
import { IAppState } from "./state";
import * as _ from "lodash";
import { FetchSelectors } from "./selectors";

export enum ActionStatus {
  RUNNING = "RUNNING",
  SUCCESS = "SUCCESS",
  FAILED = "FAILED",
}

export abstract class RestAction {
  public static isInstance(action: Action, type: string) {
    return action.type.startsWith(type);
  }
  public static implements(action: Action, type: string) {
    return action.type.indexOf(type) !== -1;
  }
}

export abstract class StatusAction {
  public static TYPE = "STATUS_ACTION/";

  status: ActionStatus;
  originalAction: any;
  exception: any;
}

// ONE OFF REQUESTS
type RequestExecutor<T> = () => Promise<T>;

export abstract class OneOffRequestAction {
  public static TYPE = "ONE_OFF/";
  type: string;
  [extraProps: string]: unknown;
}

export class SendRequestAction extends OneOffRequestAction implements Action {
  public static TYPE = OneOffRequestAction.TYPE + "SEND/";
  requestId: string;
  type: string;

  executor: RequestExecutor<any>;
  completer: PromiseCompleter<any>;
}

export class SendRequestActionStatus
  extends OneOffRequestAction
  implements StatusAction, Action {
  public static TYPE = OneOffRequestAction.TYPE + StatusAction.TYPE;

  status: ActionStatus;
  originalAction: SendRequestAction;
  result: any;
  exception: any;
}

@Injectable()
export class OneOffActions {
  sendRequest<T>(
    store: Store<IAppState>,
    requestId: string,
    executor: RequestExecutor<T>
  ): Promise<T> {
    const completer = PromiseWrapper.completer<T>();
    const action: SendRequestAction = {
      type: SendRequestAction.TYPE,
      requestId: requestId,
      executor: executor,
      completer: completer,
    };
    store.dispatch(action);
    return completer.promise;
  }

  sendRequestActionStatus(
    store: Store<IAppState>,
    originalAction: SendRequestAction,
    status: ActionStatus,
    result?: any,
    exception?: any
  ) {
    const action: SendRequestActionStatus = {
      type: SendRequestActionStatus.TYPE,
      originalAction: originalAction,
      status: status,
      result: result,
      exception: exception,
    };
    store.dispatch(action);
  }
}

// RESOURCE STATE ACTIONS

export abstract class ResourceStateAction {
  public static TYPE = "RESOURCE_STATE/";
  type: string;
  resourceName: string;

  constructor(resorceName: string) {
    this.resourceName = this.resourceName;
  }
}

export class ClearResourcesAction {
  public static TYPE = "CLEAR_RESOURCES/";
  type: string;
  resourceName: string;
  [extraProps: string]: unknown;
}

// FETCH ITEMS
export abstract class ResourceFetchAction extends ResourceStateAction {
  public static TYPE = ResourceStateAction.TYPE + "FETCH/";
  [extraProps: string]: unknown;
}

export enum RefreshOptions {
  NO,
  FULL,
  BACKGROUND,
}

export abstract class FetchItemsAction
  extends ResourceFetchAction
  implements Action {
  public static TYPE = ResourceFetchAction.TYPE + "FETCH/";

  refresh: RefreshOptions;
  params: { [key: string]: string };
  pageNumber: number;
  paginationType: PaginationType;
  completer: PromiseCompleter<BaseModel[]>;
}

export abstract class FetchItemsStatusAction
  extends ResourceFetchAction
  implements StatusAction, Action {
  public static TYPE = ResourceFetchAction.TYPE + StatusAction.TYPE;

  status: ActionStatus;
  originalAction: FetchItemsAction;
  items: BaseModel[] | undefined;
  totalItemsCount: number | undefined;
  exception: any;
}

@Injectable()
export class FetchItemsActions {
  constructor(
    private apiMapping: IAPIMapping,
    private fetchSelectors: FetchSelectors
  ) { }

  fetchItems(
    store: Store<any>,
    resourceName: string,
    refresh: RefreshOptions = RefreshOptions.NO,
    pageNumber: number = -1,
    params: { [key: string]: string } | undefined = undefined
  ): Promise<BaseModel[]> {
    const completer = PromiseWrapper.completer<BaseModel[]>();

    if (refresh !== RefreshOptions.NO) {
      if (typeof params === "undefined") {
        params = this.fetchSelectors.getCurrentParams(
          store.getState(),
          resourceName
        );
      }
    }

    const action: FetchItemsAction = {
      type: FetchItemsAction.TYPE,
      resourceName: resourceName,
      refresh: refresh,
      params: params ?? {},
      pageNumber: pageNumber,
      paginationType: this.apiMapping.getConfig(resourceName).paginationType,
      completer: completer,
    };

    store.dispatch(action);
    return action.completer.promise;
  }

  fetchMoreItemsStatus(
    store: Store<any>,
    resourceName: string,
    originalAction: FetchItemsAction,
    status: ActionStatus,
    items?: BaseModel[],
    totalItemsCount?: number,
    exception?: any
  ) {
    const action: FetchItemsStatusAction = {
      type: FetchItemsStatusAction.TYPE,
      resourceName: resourceName,
      status: status,
      items: items,
      totalItemsCount: totalItemsCount,
      originalAction: originalAction,
      exception: exception,
    };
    store.dispatch(action);
  }

  clearItems(store: Store<any>, resourceName: string): void {
    const action: ClearResourcesAction = {
      type: ClearResourcesAction.TYPE,
      resourceName: resourceName,
    };
    store.dispatch(action);
  }
}

// CREATE ITEM
export abstract class ResourceCreateAction extends ResourceStateAction {
  public static TYPE = ResourceStateAction.TYPE + "CREATE/";
  [extraProps: string]: unknown;
}

export class CreateItemAction extends ResourceCreateAction implements Action {
  public static TYPE = ResourceCreateAction.TYPE + "CREATE/";

  item: BaseModel;
  params: { [key: string]: string };
  completer: PromiseCompleter<BaseModel>;
}

export class CreateItemStatusAction
  extends ResourceCreateAction
  implements StatusAction, Action {
  public static TYPE = ResourceCreateAction.TYPE + StatusAction.TYPE;

  status: ActionStatus;
  originalAction: CreateItemAction;
  item: BaseModel | undefined;
  exception: any;
}

// -- BULK CREATE ITEMS
export abstract class ResourceBulkCreateAction extends ResourceStateAction {
  public static TYPE = ResourceStateAction.TYPE + "BULK_CREATE/";
  [extraProps: string]: unknown;
}

export class BulkCreateItemsAction
  extends ResourceBulkCreateAction
  implements Action {
  public static TYPE = ResourceBulkCreateAction.TYPE + "CREATE/";

  items: BaseModel[];
  params: { [key: string]: string };
  completer: PromiseCompleter<BaseModel[]>;
}

export class BulkCreateItemsStatusAction
  extends ResourceBulkCreateAction
  implements StatusAction, Action {
  public static TYPE = ResourceBulkCreateAction.TYPE + StatusAction.TYPE;

  status: ActionStatus;
  originalAction: BulkCreateItemsAction;
  items: BaseModel[] | undefined;
  exception: any;
}

@Injectable()
export class CreateActions {
  createItem(
    store: Store<IAppState>,
    resourceName: string,
    item: BaseModel,
    params: { [key: string]: string } = {}
  ): Promise<BaseModel> {
    const completer = PromiseWrapper.completer<BaseModel>();
    const action: CreateItemAction = {
      type: CreateItemAction.TYPE,
      resourceName: resourceName,
      item: item,
      params: params,
      completer: completer,
    };
    store.dispatch(action);
    return completer.promise;
  }

  createItemStatus(
    store: Store<any>,
    resourceName: string,
    originalAction: CreateItemAction,
    status: ActionStatus,
    item?: BaseModel,
    exception?: any
  ) {
    const action: CreateItemStatusAction = {
      type: CreateItemStatusAction.TYPE,
      resourceName: resourceName,
      status: status,
      originalAction: originalAction,
      item: item,
      exception: exception,
    };
    store.dispatch(action);
  }

  bulkCreateItems(
    store: Store<IAppState>,
    resourceName: string,
    items: BaseModel[],
    params: { [key: string]: string } = {}
  ): Promise<BaseModel[]> {
    const completer = PromiseWrapper.completer<BaseModel[]>();
    const action: BulkCreateItemsAction = {
      type: BulkCreateItemsAction.TYPE,
      resourceName: resourceName,
      items: items,
      params: params,
      completer: completer,
    };
    store.dispatch(action);
    return completer.promise;
  }

  bulkCreateItemsStatus(
    store: Store<any>,
    resourceName: string,
    originalAction: BulkCreateItemsAction,
    status: ActionStatus,
    items?: BaseModel[],
    exception?: any
  ) {
    const action: BulkCreateItemsStatusAction = {
      type: BulkCreateItemsStatusAction.TYPE,
      resourceName: resourceName,
      status: status,
      originalAction: originalAction,
      items: items,
      exception: exception,
    };
    store.dispatch(action);
  }

  forceCreateItem(
    store: Store<IAppState>,
    resourceName: string,
    item: BaseModel
  ): Promise<BaseModel> {
    const completer = PromiseWrapper.completer<BaseModel>();
    const action: CreateItemStatusAction = {
      type: UpdateItemStatusAction.TYPE,
      resourceName: resourceName,
      status: ActionStatus.SUCCESS,
      originalAction: {
        type: CreateItemAction.TYPE,
        resourceName: resourceName,
        item: _.cloneDeep(item),
        params: {},
        completer: completer,
      },
      item: item,
      exception: null,
    };
    store.dispatch(action);
    return completer.promise;
  }
}

// RETRIEVE ITEM
export abstract class ResourceRetrieveAction extends ResourceStateAction {
  public static TYPE = ResourceStateAction.TYPE + "RETRIEVE/";
  [extraProps: string]: unknown;
}

export class RetrieveItemAction
  extends ResourceRetrieveAction
  implements Action {
  public static TYPE = ResourceRetrieveAction.TYPE + "RETRIEVE/";
  id: string;
  forceRetrieve: boolean;
  params: { [key: string]: string };
  completer: PromiseCompleter<BaseModel>;
}

export class RetrieveItemStatusAction
  extends ResourceRetrieveAction
  implements StatusAction, Action {
  public static TYPE = ResourceRetrieveAction.TYPE + "STATUS/";

  status: ActionStatus;
  originalAction: RetrieveItemAction;
  item: BaseModel | undefined;
  exception: any;
}

@Injectable()
export class RetrieveActions {
  retrieveItem(
    store: Store<IAppState>,
    resourceName: string,
    id: string,
    forceRetrieve: boolean = false,
    params: { [key: string]: string } = {}
  ): Promise<BaseModel> {
    const completer = PromiseWrapper.completer<BaseModel>();
    const action: RetrieveItemAction = {
      type: RetrieveItemAction.TYPE,
      resourceName: resourceName,
      id: id,
      forceRetrieve: forceRetrieve,
      params: params,
      completer: completer,
    };
    store.dispatch(action);
    return completer.promise;
  }

  retrieveItemStatus(
    store: Store<any>,
    resourceName: string,
    originalAction: RetrieveItemAction,
    status: ActionStatus,
    item?: BaseModel,
    exception?: any
  ) {
    const action: RetrieveItemStatusAction = {
      type: RetrieveItemStatusAction.TYPE,
      resourceName: resourceName,
      status: status,
      originalAction: originalAction,
      item: item,
      exception: exception,
    };
    store.dispatch(action);
  }
}

// UPDATE
export abstract class ResourceUpdateAction extends ResourceStateAction {
  public static TYPE = ResourceStateAction.TYPE + "UPDATE/";
  [extraProps: string]: unknown;
}

export class UpdateItemAction extends ResourceUpdateAction implements Action {
  public static TYPE = ResourceUpdateAction.TYPE + "UPDATE/";

  item: BaseModel;
  params: { [key: string]: string };
  completer: PromiseCompleter<BaseModel>;
}

export class UpdateItemStatusAction
  extends ResourceUpdateAction
  implements StatusAction, Action {
  public static TYPE = ResourceUpdateAction.TYPE + StatusAction.TYPE;

  status: ActionStatus;
  originalAction: UpdateItemAction;
  item: BaseModel | undefined;
  exception: any;
}

@Injectable()
export class UpdateActions {
  updateItem(
    store: Store<IAppState>,
    resourceName: string,
    item: BaseModel,
    params: { [key: string]: string } = {}
  ): Promise<BaseModel> {
    const completer = PromiseWrapper.completer<BaseModel>();
    const action: UpdateItemAction = {
      type: UpdateItemAction.TYPE,
      resourceName: resourceName,
      item: _.cloneDeep(item),
      params: params,
      completer: completer,
    };
    store.dispatch(action);
    return completer.promise;
  }

  updateItemStatus(
    store: Store<any>,
    resourceName: string,
    originalAction: UpdateItemAction,
    status: ActionStatus,
    item?: BaseModel,
    exception?: any
  ) {
    const action: UpdateItemStatusAction = {
      type: UpdateItemStatusAction.TYPE,
      resourceName: resourceName,
      status: status,
      originalAction: originalAction,
      item: item,
      exception: exception,
    };
    store.dispatch(action);
  }

  forceUpdateItem(store: Store<any>, resourceName: string, item: BaseModel) {
    const completer = PromiseWrapper.completer<BaseModel>();
    const action: UpdateItemStatusAction = {
      type: UpdateItemStatusAction.TYPE,
      resourceName: resourceName,
      status: ActionStatus.SUCCESS,
      originalAction: {
        type: UpdateItemAction.TYPE,
        resourceName: resourceName,
        item: _.cloneDeep(item),
        params: {},
        completer: completer,
      },
      item: item,
      exception: null,
    };
    store.dispatch(action);
    return completer.promise;
  }
}

// DELETE
export abstract class ResourceDeleteAction extends ResourceStateAction {
  public static TYPE = ResourceStateAction.TYPE + "DELETE/";
  [extraProps: string]: unknown;
}

export class DeleteItemAction extends ResourceDeleteAction implements Action {
  public static TYPE = ResourceDeleteAction.TYPE + "DELETE/";

  item: BaseModel;
  params: { [key: string]: string };
  completer: PromiseCompleter<BaseModel>;
}

export class DeleteItemStatusAction
  extends ResourceDeleteAction
  implements StatusAction, Action {
  public static TYPE = ResourceDeleteAction.TYPE + StatusAction.TYPE;

  status: ActionStatus;
  item: BaseModel | undefined;
  originalAction: DeleteItemAction;
  exception: any;
}

@Injectable()
export class DeleteActions {
  deleteItem(
    store: Store<IAppState>,
    resourceName: string,
    item: BaseModel,
    params: { [key: string]: string } = {}
  ): Promise<BaseModel> {
    const completer = PromiseWrapper.completer<BaseModel>();
    const action: DeleteItemAction = {
      type: DeleteItemAction.TYPE,
      resourceName: resourceName,
      item: _.cloneDeep(item),
      params: params,
      completer: completer,
    };
    store.dispatch(action);
    return completer.promise;
  }

  deleteItemStatus(
    store: Store<any>,
    resourceName: string,
    originalAction: DeleteItemAction,
    status: ActionStatus,
    item?: BaseModel,
    exception?: any
  ) {
    const action: DeleteItemStatusAction = {
      type: DeleteItemStatusAction.TYPE,
      resourceName: resourceName,
      status: status,
      originalAction: originalAction,
      item: item,
      exception: exception,
    };
    store.dispatch(action);
  }

  forceDeleteItem(store: Store<any>, resourceName: string, item: BaseModel) {
    const completer = PromiseWrapper.completer<BaseModel>();
    const action: DeleteItemStatusAction = {
      type: DeleteItemStatusAction.TYPE,
      resourceName: resourceName,
      status: ActionStatus.SUCCESS,
      originalAction: {
        type: DeleteItemAction.TYPE,
        resourceName: resourceName,
        item: _.cloneDeep(item),
        params: {},
        completer: completer,
      },
      item: item,
      exception: null,
    };
    store.dispatch(action);
    return completer.promise;
  }
}
