import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RestMiddleware } from "./middleware";
import {
  FetchItemsActions,
  OneOffActions,
  RetrieveActions,
  UpdateActions,
  CreateActions,
  DeleteActions
} from "./actions";
import {
  FetchSelectors,
  OneOffSelectors,
  RetrieveSelectors,
  UpdateSelectors,
  CreateSelectors,
  DeleteSelectors
} from "./selectors";

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [
    RestMiddleware,
    OneOffActions,
    OneOffSelectors,
    FetchItemsActions,
    FetchSelectors,
    CreateActions,
    CreateSelectors,
    RetrieveActions,
    RetrieveSelectors,
    UpdateActions,
    UpdateSelectors,
    DeleteActions,
    DeleteSelectors
  ]
})
export class RestModule {}
