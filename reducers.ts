import * as _ from "lodash";
import { Action } from "redux";
import {
    ActionStatus,
    BulkCreateItemsStatusAction,
    ClearResourcesAction,
    CreateItemStatusAction,
    DeleteItemStatusAction,
    FetchItemsStatusAction,
    OneOffRequestAction,
    RefreshOptions,
    ResourceBulkCreateAction,
    ResourceCreateAction,
    ResourceDeleteAction,
    ResourceFetchAction,
    ResourceRetrieveAction,
    ResourceStateAction,
    ResourceUpdateAction,
    RestAction,
    RetrieveItemStatusAction,
    SendRequestAction,
    SendRequestActionStatus,
    StatusAction,
    UpdateItemStatusAction,
} from "./actions";
import { BaseModel } from "./base_model";
import { PaginationType } from "./config";
import {
    OneOffRequestState,
    ResourceBulkCreateState,
    ResourceCreateState,
    ResourceDeleteState,
    ResourceFetchState,
    ResourceRetrieveState,
    ResourceState,
    ResourceUpdateState,
    RestState,
} from "./state";
import { PromiseCompleter } from "./utils";

export function restReducer(state: RestState, action: Action): RestState {
    return {
        resourceState: resourceStateReducer(state.resourceState, action),
        oneOffRequestState: oneOffRequestReducer(
            state.oneOffRequestState,
            action
        ),
    };
}

// MAIN REDUCERS
function resourceStateReducer(
    state: { [resourceName: string]: ResourceState },
    action: any
): { [resourceName: string]: ResourceState } {
    if (RestAction.isInstance(action, ResourceStateAction.TYPE)) {
        const stateAction = action as ResourceStateAction;
        const newState = _.clone(state);
        const oldResourceState =
            state[stateAction.resourceName] || new ResourceState();

        const newItemsBydId = itemsByIdReducer(
            oldResourceState.itemsById,
            action
        );
        const newItemIds = itemIdsReducer(oldResourceState.itemIds, action);
        const newItems = itemsReducer(
            oldResourceState.items,
            newItemIds,
            newItemsBydId,
            action
        );

        const newResourceState = {
            itemId: itemIdReducer(oldResourceState.itemId, action),
            itemsById: newItemsBydId,
            itemIds: newItemIds,
            items: newItems,
            totalItemsCount: totalItemsCountReducer(
                oldResourceState.totalItemsCount,
                action
            ),
            resourceFetchState: resourceFetchReducer(
                oldResourceState.resourceFetchState,
                action
            ),
            resourceCreateState: resourceCreateStateReducer(
                oldResourceState.resourceCreateState,
                action
            ),
            resourceBulkCreateState: resourceBulkCreateStateReducer(
                oldResourceState.resourceBulkCreateState,
                action
            ),
            resourceRetrieveState: resourceRetrieveStateReducer(
                oldResourceState.resourceRetrieveState,
                action
            ),
            resourceUpdateState: resourceUpdateStateReducer(
                oldResourceState.resourceUpdateState,
                action
            ),
            resourceDeleteState: resourceDeleteStateReducer(
                oldResourceState.resourceDeleteState,
                action
            ),
        };

        newState[stateAction.resourceName] = newResourceState;
        return newState;
    } else if (RestAction.isInstance(action, ClearResourcesAction.TYPE)) {
        const newState = _.clone(state);
        const clearAction = action as ClearResourcesAction;
        newState[clearAction.resourceName] = new ResourceState();
        return newState;
    } else {
        return state;
    }
}

function oneOffRequestReducer(
    state: { [key: string]: OneOffRequestState },
    action: any
): { [key: string]: OneOffRequestState } {
    if (RestAction.isInstance(action, OneOffRequestAction.TYPE)) {
        const newState = _.clone(state);
        const requestId = RestAction.isInstance(action, SendRequestAction.TYPE)
            ? action.requestId
            : action.originalAction.requestId;
        const oldOneOffRequestState =
            newState[requestId] || new OneOffRequestState();

        const newOneOffRequestState = {
            isRunning: _boolStatusReducer(
                oldOneOffRequestState.isRunning,
                action,
                SendRequestActionStatus
            ),
            result: requestResultReducer(oldOneOffRequestState.result, action),
            exception: _exceptionReducer(
                oldOneOffRequestState.exception,
                action,
                SendRequestActionStatus
            ),
            failedAction: _failedActionReducer(
                oldOneOffRequestState.failedAction,
                action,
                SendRequestActionStatus
            ),
        };

        newState[requestId] = newOneOffRequestState;
        return newState;
    }
    return state;
}

// ONE OFF REQUESTS
function requestResultReducer(state: any, action: any): any {
    if (
        RestAction.isInstance(action, SendRequestActionStatus.TYPE) &&
        action.status === ActionStatus.RUNNING
    ) {
        return null;
    } else if (
        RestAction.isInstance(action, SendRequestActionStatus.TYPE) &&
        action.status === ActionStatus.SUCCESS
    ) {
        return action.result;
    } else {
        return state;
    }
}

// RESOURCE STATE

function itemsByIdReducer(
    state: { [resourceName: string]: BaseModel },
    action: any
): { [resourceName: string]: BaseModel } {
    if (
        (RestAction.isInstance(action, FetchItemsStatusAction.TYPE) ||
            RestAction.isInstance(action, BulkCreateItemsStatusAction.TYPE)) &&
        (action as StatusAction).status === ActionStatus.SUCCESS
    ) {
        let itemsById: { [key: string]: any } = {};
        if (RestAction.isInstance(action, FetchItemsStatusAction.TYPE)) {
            const statusAction = action as FetchItemsStatusAction;
            if (
                statusAction.originalAction.refresh === RefreshOptions.NO &&
                statusAction.originalAction.paginationType ===
                    PaginationType.ALWAYS_APPEND
            ) {
                itemsById = _.cloneDeep(state);
            } else {
                itemsById = {};
            }
        } else if (
            RestAction.isInstance(action, BulkCreateItemsStatusAction.TYPE)
        ) {
            itemsById = _.cloneDeep(state);
        }
        for (const item of action.items) {
            itemsById[item.id] = item;
        }

        return itemsById;
    } else if (
        (RestAction.isInstance(action, RetrieveItemStatusAction.TYPE) ||
            RestAction.isInstance(action, CreateItemStatusAction.TYPE) ||
            RestAction.isInstance(action, UpdateItemStatusAction.TYPE)) &&
        action.status === ActionStatus.SUCCESS
    ) {
        const itemsById = _.cloneDeep(state);

        itemsById[action.item.id] = action.item;

        return itemsById;
    } else if (
        RestAction.isInstance(action, DeleteItemStatusAction.TYPE) &&
        action.status === ActionStatus.SUCCESS
    ) {
        const itemsById = _.cloneDeep(state);
        delete itemsById[action.item.id];
        return itemsById;
    } else {
        return state;
    }
}

// FETCH ITEMS
function resourceFetchReducer(
    state: ResourceFetchState,
    action: Action
): ResourceFetchState {
    if (RestAction.isInstance(action, ResourceFetchAction.TYPE)) {
        const newState = {
            exception: _exceptionReducer(
                state.exception,
                action,
                FetchItemsStatusAction
            ),
            failedAction: _failedActionReducer(
                state.failedAction,
                action,
                FetchItemsStatusAction
            ),
            loadedItems: loadedItemsReducer(state.loadedItems, action),
            isRunning: _boolStatusReducer(
                state.isRunning,
                action,
                FetchItemsStatusAction
            ),
            currentPage: currentPageReducer(state.currentPage, action),
            currentParams: currentParamsReducer(state.currentParams, action),
        };
        return newState;
    } else {
        return state;
    }
}

function itemIdsReducer(state: string[], action: any): string[] {
    let itemIds: string[] = [];
    if (
        (RestAction.isInstance(action, FetchItemsStatusAction.TYPE) ||
            RestAction.isInstance(action, BulkCreateItemsStatusAction.TYPE)) &&
        action.status === ActionStatus.SUCCESS
    ) {
        if (RestAction.isInstance(action, FetchItemsStatusAction.TYPE)) {
            if (
                action.originalAction.refresh === RefreshOptions.NO &&
                action.originalAction.paginationType ===
                    PaginationType.ALWAYS_APPEND
            ) {
                itemIds = _.cloneDeep(state);
            } else {
                itemIds = [];
            }
        } else if (
            RestAction.isInstance(action, BulkCreateItemsStatusAction.TYPE)
        ) {
            itemIds = _.cloneDeep(state);
        }
        for (const item of action.items) {
            if (itemIds.indexOf(item.id) === -1) {
                itemIds.push(item.id);
            }
        }

        return itemIds;
    } else if (
        RestAction.isInstance(action, DeleteItemStatusAction.TYPE) &&
        action.status === ActionStatus.SUCCESS
    ) {
        const itemIds = state.filter((x) => x !== action.item.id);
        return itemIds;
    } else if (
        RestAction.isInstance(action, CreateItemStatusAction.TYPE) &&
        action.status === ActionStatus.SUCCESS
    ) {
        itemIds = _.cloneDeep(state);
        itemIds.unshift(action.item.id);
        return itemIds;
    } else {
        return state;
    }
}

function itemsReducer(
    state: BaseModel[],
    newItemIds: string[],
    newItemsById: { [itemId: string]: BaseModel },
    action: any
): BaseModel[] {
    if (
        (RestAction.isInstance(action, FetchItemsStatusAction.TYPE) &&
            action.status === ActionStatus.SUCCESS) ||
        ((RestAction.isInstance(action, CreateItemStatusAction.TYPE) ||
            RestAction.isInstance(action, BulkCreateItemsStatusAction.TYPE) ||
            RestAction.isInstance(action, RetrieveItemStatusAction.TYPE) ||
            RestAction.isInstance(action, UpdateItemStatusAction.TYPE) ||
            RestAction.isInstance(action, DeleteItemStatusAction.TYPE)) &&
            action.status === ActionStatus.SUCCESS)
    ) {
        const items: BaseModel[] = [];
        for (const itemId of newItemIds) {
            const item = newItemsById[itemId];
            if (item == null) {
                throw new Error(
                    `Item with id "${itemId}" was listed but not found in mapping for resource "$resourceName".`
                );
            }
            items.push(_.cloneDeep(item));
        }

        return items;
    }
    return state;
}

function loadedItemsReducer(state: boolean, action: any) {
    if (
        RestAction.isInstance(action, FetchItemsStatusAction.TYPE) &&
        action.status === ActionStatus.SUCCESS
    ) {
        return true;
    } else if (
        RestAction.isInstance(action, FetchItemsStatusAction.TYPE) &&
        action.status === ActionStatus.RUNNING &&
        action.originalAction.refresh === RefreshOptions.FULL
    ) {
        return false;
    } else {
        return state;
    }
}

function totalItemsCountReducer(state: number, action: any): number {
    if (
        RestAction.isInstance(action, FetchItemsStatusAction.TYPE) &&
        action.status === ActionStatus.RUNNING &&
        action.originalAction.refresh === RefreshOptions.FULL
    ) {
        return -1;
    } else if (
        RestAction.isInstance(action, FetchItemsStatusAction.TYPE) &&
        action.status === ActionStatus.SUCCESS
    ) {
        return action.totalItemsCount;
    } else if (
        RestAction.isInstance(action, BulkCreateItemsStatusAction.TYPE) &&
        action.status === ActionStatus.SUCCESS
    ) {
        return state + action.items.length;
    } else if (
        RestAction.isInstance(action, CreateItemStatusAction.TYPE) &&
        action.status === ActionStatus.SUCCESS
    ) {
        return state + 1;
    } else if (
        RestAction.isInstance(action, DeleteItemStatusAction.TYPE) &&
        action.status === ActionStatus.SUCCESS
    ) {
        return state - 1;
    } else {
        return state;
    }
}

function currentPageReducer(state: number, action: any): number {
    if (
        RestAction.isInstance(action, FetchItemsStatusAction.TYPE) &&
        action.status === ActionStatus.SUCCESS
    ) {
        return action.originalAction.pageNumber;
    } else {
        return state;
    }
}
function currentParamsReducer(
    state: { [key: string]: string },
    action: any
): { [key: string]: string } {
    if (
        RestAction.isInstance(action, FetchItemsStatusAction.TYPE) &&
        action.status === ActionStatus.SUCCESS
    ) {
        return action.originalAction.params;
    } else {
        return state;
    }
}
// CREATE/RETRIEVE/UPDATE/DELETE ITEM
function itemIdReducer(
    state: string | undefined,
    action: any
): string | undefined {
    if (
        (RestAction.isInstance(action, CreateItemStatusAction.TYPE) ||
            RestAction.isInstance(action, RetrieveItemStatusAction.TYPE)) &&
        action.status === ActionStatus.RUNNING
    ) {
        return undefined;
    } else if (
        (RestAction.isInstance(action, CreateItemStatusAction.TYPE) ||
            RestAction.isInstance(action, UpdateItemStatusAction.TYPE) ||
            RestAction.isInstance(action, RetrieveItemStatusAction.TYPE)) &&
        action.status === ActionStatus.SUCCESS
    ) {
        return action.item.id;
    } else if (
        RestAction.isInstance(action, DeleteItemStatusAction.TYPE) &&
        action.status === ActionStatus.SUCCESS
    ) {
        return undefined;
    } else {
        return state;
    }
}

// CREATE ITEM
function resourceCreateStateReducer(
    state: ResourceCreateState,
    action: Action
): ResourceCreateState {
    if (RestAction.isInstance(action, ResourceCreateAction.TYPE)) {
        return {
            isRunning: _boolStatusReducer(
                state.isRunning,
                action,
                CreateItemStatusAction
            ),
            exception: _exceptionReducer(
                state.exception,
                action,
                CreateItemStatusAction
            ),
            failedAction: _failedActionReducer(
                state.failedAction,
                action,
                CreateItemStatusAction
            ),
        };
    } else {
        return state;
    }
}

// BULK CREATE ITEMS
function resourceBulkCreateStateReducer(
    state: ResourceBulkCreateState,
    action: Action
): ResourceBulkCreateState {
    if (RestAction.isInstance(action, ResourceBulkCreateAction.TYPE)) {
        return {
            isRunning: _boolStatusReducer(
                state.isRunning,
                action,
                BulkCreateItemsStatusAction
            ),
            exception: _exceptionReducer(
                state.exception,
                action,
                BulkCreateItemsStatusAction
            ),
            failedAction: _failedActionReducer(
                state.failedAction,
                action,
                BulkCreateItemsStatusAction
            ),
        };
    } else {
        return state;
    }
}

// RETRIEVE ITEM
function resourceRetrieveStateReducer(
    state: ResourceRetrieveState,
    action: Action
): ResourceRetrieveState {
    if (RestAction.isInstance(action, ResourceRetrieveAction.TYPE)) {
        return {
            isRunning: _boolStatusReducer(
                state.isRunning,
                action,
                RetrieveItemStatusAction
            ),
            exception: _exceptionReducer(
                state.exception,
                action,
                RetrieveItemStatusAction
            ),
            failedAction: _failedActionReducer(
                state.failedAction,
                action,
                RetrieveItemStatusAction
            ),
        };
    } else {
        return state;
    }
}

// UPDATE ITEM
function resourceUpdateStateReducer(
    state: ResourceUpdateState,
    action: Action
): ResourceUpdateState {
    if (RestAction.isInstance(action, ResourceUpdateAction.TYPE)) {
        return {
            isRunning: _boolStatusReducer(
                state.isRunning,
                action,
                UpdateItemStatusAction
            ),
            exception: _exceptionReducer(
                state.exception,
                action,
                UpdateItemStatusAction
            ),
            failedAction: _failedActionReducer(
                state.failedAction,
                action,
                UpdateItemStatusAction
            ),
        };
    } else {
        return state;
    }
}

// DELETE ITEM

function resourceDeleteStateReducer(
    state: ResourceDeleteState,
    action: Action
): ResourceDeleteState {
    if (RestAction.isInstance(action, ResourceDeleteAction.TYPE)) {
        return {
            isRunning: _boolStatusReducer(
                state.isRunning,
                action,
                DeleteItemStatusAction
            ),
            exception: _exceptionReducer(
                state.exception,
                action,
                DeleteItemStatusAction
            ),
            failedAction: _failedActionReducer(
                state.failedAction,
                action,
                DeleteItemStatusAction
            ),
        };
    } else {
        return state;
    }
}

function _boolStatusReducer(state: boolean, action: any, actionType: any) {
    if (
        RestAction.isInstance(action, actionType.TYPE) &&
        action.status === ActionStatus.RUNNING
    ) {
        return true;
    } else if (
        RestAction.isInstance(action, actionType.TYPE) &&
        (action.status === ActionStatus.SUCCESS ||
            action.status === ActionStatus.FAILED)
    ) {
        // Completing futures
        if (
            (RestAction.isInstance(action, FetchItemsStatusAction.TYPE) ||
                RestAction.isInstance(
                    action,
                    BulkCreateItemsStatusAction.TYPE
                )) &&
            action.status === ActionStatus.SUCCESS
        ) {
            action.originalAction.completer.resolve(action.items);
        } else if (
            (RestAction.isInstance(action, CreateItemStatusAction.TYPE) ||
                RestAction.isInstance(action, RetrieveItemStatusAction.TYPE) ||
                RestAction.isInstance(action, UpdateItemStatusAction.TYPE) ||
                RestAction.isInstance(action, DeleteItemStatusAction.TYPE)) &&
            action.status === ActionStatus.SUCCESS
        ) {
            (action.originalAction.completer as PromiseCompleter<any>).resolve(
                action.item
            );
        } else if (
            RestAction.isInstance(action, SendRequestActionStatus.TYPE) &&
            action.status === ActionStatus.SUCCESS
        ) {
            action.originalAction.completer.resolve(action.result);
        } else if (
            RestAction.implements(action, StatusAction.TYPE) &&
            action.status === ActionStatus.FAILED
        ) {
            if (!action.originalAction.completer.isCompleted) {
                action.originalAction.completer.reject(action.exception);
            } else {
                throw action.exception;
            }
        }
        return false;
    } else {
        return state;
    }
}

function _exceptionReducer(state: any, action: any, actionType: any) {
    if (
        RestAction.isInstance(action, actionType.TYPE) &&
        action.status === ActionStatus.FAILED
    ) {
        return action.exception;
    } else if (RestAction.isInstance(action, actionType.TYPE)) {
        return null;
    } else {
        return state;
    }
}

function _failedActionReducer(state: any, action: any, actionType: any) {
    if (
        RestAction.isInstance(action, actionType.TYPE) &&
        action.status === ActionStatus.FAILED
    ) {
        return action.originalAction;
    } else if (RestAction.isInstance(action, actionType.TYPE)) {
        return null;
    } else {
        return state;
    }
}
