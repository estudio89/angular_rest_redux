import { Injectable } from "@angular/core";
import { Store } from "redux";
import { IAppState } from "src/app/rest/state";
import {
    ActionStatus,
    BulkCreateItemsAction,
    CreateActions,
    CreateItemAction,
    DeleteActions,
    DeleteItemAction,
    FetchItemsAction,
    FetchItemsActions,
    OneOffActions,
    RestAction,
    RetrieveActions,
    RetrieveItemAction,
    SendRequestAction,
    UpdateActions,
    UpdateItemAction,
} from "./actions";
import { FetchResult, ValidationError } from "./base_api";
import { BaseModel } from "./base_model";
import { IAPIMapping } from "./config";
import {
    FetchSelectors,
    OneOffSelectors,
    RetrieveSelectors,
} from "./selectors";

@Injectable()
export class RestMiddleware {
    constructor(
        private apiMapping: IAPIMapping,
        private oneOffSelectors: OneOffSelectors,
        private oneOffActions: OneOffActions,
        private fetchSelectors: FetchSelectors,
        private fetchItemsActions: FetchItemsActions,
        private createActions: CreateActions,
        private retrieveActions: RetrieveActions,
        private retrieveSelectors: RetrieveSelectors,
        private updateActions: UpdateActions,
        private deleteActions: DeleteActions
    ) {}

    getMiddleware(): any[] {
        return [
            new OneOffRequestsMiddleware(
                this.oneOffSelectors,
                this.oneOffActions
            ).middleware,
            new FetchItemsMiddleware(
                this.apiMapping,
                this.fetchItemsActions,
                this.fetchSelectors
            ).middleware,
            new CreateItemMiddleware(this.apiMapping, this.createActions)
                .middleware,
            new RetrieveItemMiddleware(
                this.apiMapping,
                this.retrieveActions,
                this.retrieveSelectors
            ).middleware,
            new UpdateItemMiddleware(this.apiMapping, this.updateActions)
                .middleware,
            new DeleteItemMiddleware(this.apiMapping, this.deleteActions)
                .middleware,
        ];
    }
}

class OneOffRequestsMiddleware {
    constructor(
        private oneOffSelectors: OneOffSelectors,
        private oneOffActions: OneOffActions
    ) {}

    middleware =
        (store: Store<IAppState>) =>
        (nextMiddleware: (store: Store<IAppState>) => void) =>
        (action: any) => {
            if (RestAction.isInstance(action, SendRequestAction.TYPE)) {
                const state = store.getState();

                let ignore = false;
                if (this.oneOffSelectors.isRunning(state, action.requestId)) {
                    ignore = true;
                }

                if (!ignore) {
                    this.oneOffActions.sendRequestActionStatus(
                        store,
                        action,
                        ActionStatus.RUNNING
                    );

                    action
                        .executor()
                        .then((result: any) => {
                            this.oneOffActions.sendRequestActionStatus(
                                store,
                                action,
                                ActionStatus.SUCCESS,
                                result
                            );
                        })
                        .catch((error: any) => {
                            this.oneOffActions.sendRequestActionStatus(
                                store,
                                action,
                                ActionStatus.FAILED,
                                undefined,
                                error
                            );
                            error = _checkError(error);
                        });
                }
            }

            nextMiddleware(action);
        };
}

class FetchItemsMiddleware {
    actions: FetchItemsActions;
    constructor(
        private apiMapping: IAPIMapping,
        private fetchItemsActions: FetchItemsActions,
        private fetchSelectors: FetchSelectors
    ) {}

    middleware =
        (store: Store<IAppState>) =>
        (nextMiddleware: (store: Store<IAppState>) => void) =>
        (action: any) => {
            if (RestAction.isInstance(action, FetchItemsAction.TYPE)) {
                const state = store.getState();
                const nextPage =
                    action.pageNumber === -1
                        ? this.fetchSelectors.getNextPage(
                              state,
                              action.resourceName,
                              action.refresh
                          )
                        : action.pageNumber;
                const wantsInitialFetch: boolean = nextPage === 0;
                const wantsFetchMore: boolean = nextPage > 0;

                let ignoreLoad = false;
                if (
                    (wantsFetchMore &&
                        !this.fetchSelectors.canFetchMore(
                            state,
                            action.resourceName
                        )) ||
                    (wantsInitialFetch &&
                        !this.fetchSelectors.canFetch(
                            state,
                            action.resourceName,
                            action.refresh
                        ))
                ) {
                    ignoreLoad = true;
                }
                if (ignoreLoad) {
                    action.completer.resolve(
                        this.fetchSelectors.getItems(state, action.resourceName)
                    );
                } else {
                    this.fetchItemsActions.fetchMoreItemsStatus(
                        store,
                        action.resourceName,
                        action,
                        ActionStatus.RUNNING
                    );

                    const itemAPI = this.apiMapping.getAPI(action.resourceName);

                    itemAPI
                        .fetch(nextPage, action.params)
                        .then((fetchResult: FetchResult<BaseModel>) => {
                            this.fetchItemsActions.fetchMoreItemsStatus(
                                store,
                                action.resourceName,
                                action,
                                ActionStatus.SUCCESS,
                                fetchResult.items,
                                fetchResult.totalItemsCount
                            );
                        })
                        .catch((error: any) => {
                            this.fetchItemsActions.fetchMoreItemsStatus(
                                store,
                                action.resourceName,
                                action,
                                ActionStatus.FAILED,
                                undefined,
                                undefined,
                                error
                            );
                            error = _checkError(error);
                        });
                }
            }

            nextMiddleware(action);
        };
}

// CREATE
class CreateItemMiddleware {
    constructor(
        private apiMapping: IAPIMapping,
        private createActions: CreateActions
    ) {}

    middleware =
        (store: Store<IAppState>) =>
        (nextMiddleware: (store: Store<IAppState>) => void) =>
        (action: any) => {
            if (RestAction.isInstance(action, CreateItemAction.TYPE)) {
                this.createActions.createItemStatus(
                    store,
                    action.resourceName,
                    action,
                    ActionStatus.RUNNING
                );
                const itemAPI = this.apiMapping.getAPI(action.resourceName);

                itemAPI
                    .create(action.item, action.params)
                    .then((item: BaseModel) => {
                        this.createActions.createItemStatus(
                            store,
                            action.resourceName,
                            action,
                            ActionStatus.SUCCESS,
                            item
                        );
                    })
                    .catch((error: any) => {
                        this.createActions.createItemStatus(
                            store,
                            action.resourceName,
                            action,
                            ActionStatus.FAILED,
                            undefined,
                            error
                        );
                        error = _checkError(error);
                    });
            } else if (
                RestAction.isInstance(action, BulkCreateItemsAction.TYPE)
            ) {
                this.createActions.bulkCreateItemsStatus(
                    store,
                    action.resourceName,
                    action,
                    ActionStatus.RUNNING
                );

                const itemAPI = this.apiMapping.getAPI(action.resourceName);

                itemAPI
                    .bulkCreate(action.items, action.params)
                    .then((items: BaseModel[]) => {
                        this.createActions.bulkCreateItemsStatus(
                            store,
                            action.resourceName,
                            action,
                            ActionStatus.SUCCESS,
                            items
                        );
                    })
                    .catch((error: any) => {
                        this.createActions.bulkCreateItemsStatus(
                            store,
                            action.resourceName,
                            action,
                            ActionStatus.FAILED,
                            undefined,
                            error
                        );
                    });
            }

            nextMiddleware(action);
        };
}

// RETRIEVE
class RetrieveItemMiddleware {
    constructor(
        private apiMapping: IAPIMapping,
        private retrieveActions: RetrieveActions,
        private retrieveSelectors: RetrieveSelectors
    ) {}

    middleware =
        (store: Store<IAppState>) =>
        (nextMiddleware: (store: Store<IAppState>) => void) =>
        (action: any) => {
            if (RestAction.isInstance(action, RetrieveItemAction.TYPE)) {
                this.retrieveActions.retrieveItemStatus(
                    store,
                    action.resourceName,
                    action,
                    ActionStatus.RUNNING
                );

                let inCache = false;
                let cachedItem: BaseModel;

                try {
                    cachedItem = this.retrieveSelectors.getItem(
                        store.getState(),
                        action.resourceName,
                        action.id
                    );
                    inCache = true;
                } catch (error) { }

                if (inCache && !action.forceRetrieve) {
                    this.retrieveActions.retrieveItemStatus(
                        store,
                        action.resourceName,
                        action,
                        ActionStatus.SUCCESS,
                        cachedItem!
                    );
                } else {
                    const itemAPI = this.apiMapping.getAPI(action.resourceName);

                    itemAPI
                        .retrieve(action.id, action.params)
                        .then((item: BaseModel) => {
                            this.retrieveActions.retrieveItemStatus(
                                store,
                                action.resourceName,
                                action,
                                ActionStatus.SUCCESS,
                                item
                            );
                        })
                        .catch((error: any) => {
                            this.retrieveActions.retrieveItemStatus(
                                store,
                                action.resourceName,
                                action,
                                ActionStatus.FAILED,
                                undefined,
                                error
                            );

                            error = _checkError(error);
                        });
                }
            }

            nextMiddleware(action);
        };
}

class UpdateItemMiddleware {
    constructor(
        private apiMapping: IAPIMapping,
        private updateActions: UpdateActions
    ) {}

    middleware =
        (store: Store<IAppState>) =>
        (nextMiddleware: (store: Store<IAppState>) => void) =>
        (action: any) => {
            if (RestAction.isInstance(action, UpdateItemAction.TYPE)) {
                this.updateActions.updateItemStatus(
                    store,
                    action.resourceName,
                    action,
                    ActionStatus.RUNNING
                );
                const itemAPI = this.apiMapping.getAPI(action.resourceName);

                itemAPI
                    .update(action.item, action.params)
                    .then((item: BaseModel) => {
                        this.updateActions.updateItemStatus(
                            store,
                            action.resourceName,
                            action,
                            ActionStatus.SUCCESS,
                            item
                        );
                    })
                    .catch((error: any) => {
                        this.updateActions.updateItemStatus(
                            store,
                            action.resourceName,
                            action,
                            ActionStatus.FAILED,
                            undefined,
                            error
                        );
                        error = _checkError(error);
                    });
            }

            nextMiddleware(action);
        };
}

class DeleteItemMiddleware {
    constructor(
        private apiMapping: IAPIMapping,
        private deleteActions: DeleteActions
    ) {}

    middleware =
        (store: Store<IAppState>) =>
        (nextMiddleware: (store: Store<IAppState>) => void) =>
        (action: any) => {
            if (RestAction.isInstance(action, DeleteItemAction.TYPE)) {
                this.deleteActions.deleteItemStatus(
                    store,
                    action.resourceName,
                    action,
                    ActionStatus.RUNNING
                );

                const itemAPI = this.apiMapping.getAPI(action.resourceName);

                itemAPI
                    .delete(action.item, action.params)
                    .then((response: any) => {
                        this.deleteActions.deleteItemStatus(
                            store,
                            action.resourceName,
                            action,
                            ActionStatus.SUCCESS,
                            action.item
                        );
                    })
                    .catch((error: any) => {
                        this.deleteActions.deleteItemStatus(
                            store,
                            action.resourceName,
                            action,
                            ActionStatus.FAILED,
                            undefined,
                            error
                        );

                        error = _checkError(error);
                    });
            }

            nextMiddleware(action);
        };
}
// DELETE
function _checkError(error: any) {
    // TODO
    // return error;
  if (error instanceof ValidationError) {
    return error;
  }
  throw error;
}
