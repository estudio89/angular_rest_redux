import { BaseModel } from "./base_model";
import { HttpClient } from "@angular/common/http";
import { HttpErrorResponse } from "@angular/common/http";
import { lastValueFrom } from "rxjs";
export class ValidationError {
  messages: string[];
  constructor(errorResponse: HttpErrorResponse) {
    let errors = errorResponse.error;
    if (Array.isArray(errors)) {
      for (const error of errors) {
        if (Object.keys(error).length > 0) {
          errors = error;
          break;
        }
      }
    }

    this.messages = [];

    if (typeof errors === "string") {
      this.messages.push(errors);
    } else {
      for (const key in errors) {
        if (errors.hasOwnProperty(key)) {
          const value = errors[key];
          this.messages = this.messages.concat(value);
        }
      }
    }
  }
}

export class FetchResponse {
  count: number;
  results: any[];
}

export class FetchResult<T extends BaseModel> {
  items: T[];
  totalItemsCount: number;
}

export enum CRUD_URL {
  FETCH = "FETCH",
  CREATE = "CREATE",
  BULK_CREATE = "CREATE",
  RETRIEVE = "RETRIEVE",
  UPDATE = "UPDATE",
  DELETE = "DELETE",
}

export function serializeQueryParameters(params: { [key: number]: string }) {
  const paramsList = [];
  for (const prop in params) {
    if (params.hasOwnProperty(prop) && !!params[prop]) {
      paramsList.push(encodeURIComponent(prop) + "=" + encodeURIComponent(params[prop]));
    }
  }
  return paramsList.join("&");
}

export abstract class BaseAPI<T extends BaseModel> {
  constructor(public client: HttpClient, protected urlMap: { [key: string]: string }) {}

  serialize(params: { [key: number]: string } = {}): string {
    return serializeQueryParameters(params);
  }

  assembleUrl(url: string, params: { [key: string]: string } = {}) {
    const queryString = this.serialize(params);
    if (queryString.length > 0) {
      return `${url}?${queryString}`;
    }
    return url;
  }

  abstract fromJson(jsonObject: any): T;

  toJson(item: T): any {
    throw new Error("Method not implemented!");
  }

  getCrudUrl(crudUrl: CRUD_URL, params: { [key: string]: string }): string {
    return this.urlMap[crudUrl];
  }

  fetch(pageNumber: number, params: { [key: string]: string }): Promise<FetchResult<T>> {
    const crudUrl = this.getCrudUrl(CRUD_URL.FETCH, params);

    if (!crudUrl) {
      throw new Error("Not implemented!");
    }

    if (!!pageNumber) {
      params["page"] = `${pageNumber + 1}`;
    }

    const url = this.assembleUrl(crudUrl, params);
    const promise = this.client
      .get<FetchResponse>(url)
      .toPromise()
      .then((data) => {
        const items: T[] = [];
        let jsonArray: any[];
        let totalItemsCount: number;
        if (data instanceof Array) {
          jsonArray = data;
          totalItemsCount = jsonArray.length;
        } else {
          jsonArray = data!.results;
          totalItemsCount = data!.count;
        }
        jsonArray.forEach((jsonObject) => {
          const item: T = this.fromJson(jsonObject);
          items.push(item);
        });

        return { totalItemsCount: totalItemsCount, items: items };
      });

    return promise;
  }

  create(item: T, params: { [key: string]: string }): Promise<T> {
    const crudUrl = this.getCrudUrl(CRUD_URL.CREATE, params);
    if (!crudUrl) {
      throw new Error("Method not implemented.");
    }
    const url = this.assembleUrl(crudUrl, params);
    const promise = this.client
      .post<any>(url, this.toJson(item))
      .toPromise()
      .then((data) => {
        return this.fromJson(data);
      })
      .catch((errorResponse) => {
        if (errorResponse.status === 400 && !!errorResponse.error) {
          const validationError = new ValidationError(errorResponse);
          throw validationError;
        }
        throw errorResponse;
      });
    return promise;
  }

  bulkCreate(items: T[], params: { [key: string]: string }): Promise<T[]> {
    const crudUrl = this.urlMap.CREATE_BULK;

    if (!crudUrl) {
      throw new Error("Method not implemented.");
    }
    const postData = [];
    for (const item of items) {
      postData.push(this.toJson(item));
    }
    const url = this.assembleUrl(crudUrl, params);
    const promise = this.client
      .post<any>(url, postData)
      .toPromise()
      .then((data) => {
        const createdItems = [];
        for (const datum of data) {
          const item = this.fromJson(datum);
          createdItems.push(item);
        }
        return createdItems;
      })
      .catch((errorResponse) => {
        if (errorResponse.status === 400 && !!errorResponse.error) {
          const validationError = new ValidationError(errorResponse);
          throw validationError;
        }
        throw errorResponse;
      });
    return promise;
  }

  retrieve(itemId: string, params: { [key: string]: string }): Promise<T> {
    const crudUrl = this.getCrudUrl(CRUD_URL.RETRIEVE, params);
    if (!crudUrl) {
      throw new Error("Not implemented!");
    }
    const url = this.assembleUrl(crudUrl.replace(":id", itemId), params);
    const promise = this.client
      .get<any>(url)
      .toPromise()
      .then((data) => {
        return this.fromJson(data);
      });
    return promise;
  }

  update(item: T, params: { [key: string]: string }): Promise<T> {
    const crudUrl = this.getCrudUrl(CRUD_URL.UPDATE, params);
    if (!crudUrl) {
      throw new Error("Method not implemented.");
    }
    const url = this.assembleUrl(crudUrl.replace(":id", item.id), params);
    const promise = this.client
      .put<any>(url, this.toJson(item))
      .toPromise()
      .then((data) => {
        return this.fromJson(data);
      })
      .catch((errorResponse) => {
        if (errorResponse.status === 400 && !!errorResponse.error) {
          const validationError = new ValidationError(errorResponse);
          throw validationError;
        }
        throw errorResponse;
      });
    return promise;
  }

  delete(item: T, params: { [key: string]: string }): Promise<any> {
    const crudUrl = this.getCrudUrl(CRUD_URL.DELETE, params);
    if (!crudUrl) {
      throw new Error("Method not implemented.");
    }
    const url = this.assembleUrl(crudUrl.replace(":id", item.id), params);

    const promise = lastValueFrom(this.client.delete<any>(url)).catch((errorResponse) => {
      if (errorResponse.status === 400 && !!errorResponse.error) {
        const validationError = new ValidationError(errorResponse);
        throw validationError;
      }
      throw errorResponse;
    });

    return promise;
  }
}
