
export abstract class IAPIMapping {
    abstract getAPI(
        resourceName: string
    ): any;

    abstract getConfig(resourceName: string): IRestResourceConfig;
}

export enum PaginationType {
    ALWAYS_APPEND,
    ALWAYS_RESET,
}

export abstract class IRestResourceConfig {
    paginationType: PaginationType = PaginationType.ALWAYS_RESET;
}
